package day6;

import java.util.Scanner;

public class latihan_soal3 {
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Enter the numbeer of case");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {

				case 1:
					soal1();
					break;

				case 2:
					soal2();
					break;

				case 3:
					soal3();
					break;

				case 4:
					soal4();
					break;

				case 5:
					soal5();
					break;

				case 6:
					soal6();
					break;

				default:
					System.out.println("case is not available");
					break;

				}

			} catch (Exception e) {
				System.out.println(e.getMessage());

			}
			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}
	}

	private static void soal1() {
		System.out.println("Disini Soal 1");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");
		System.out.println();

		System.out.print("Input: ");
		int angka = input.nextInt();
		int[] arrayAngka = new int[angka];

		for (int i = 0; i < arrayAngka.length; i++) {

			if (i > 1) {
				arrayAngka[i] = arrayAngka[i - 1] + arrayAngka[i - 2];

			} else {
				arrayAngka[i] = 1;
			}
			System.out.print(arrayAngka[i] + ",");

		}

	}

	private static void soal2() {
		System.out.println("Disini Soal 2");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");
		System.out.println();

		System.out.println("Input: ");
		int angka = input.nextInt();

		int[] arrayAngka = new int[angka];

		int temp = 0;

		for (int i = 0; i < arrayAngka.length; i++) {
			if (i > 2) {
				arrayAngka[i] = arrayAngka[i - 1] + arrayAngka[i - 2] + arrayAngka[i - 3];

			} else {
				arrayAngka[i] = 1;
			}

			System.out.print(arrayAngka[i] + ",");

		}

	}

	private static void soal3() {
		System.out.println("Disini Soal 3");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");
		System.out.println();

		System.out.print("Start From: ");
		int angka = input.nextInt();
		int bilangan = 0;
		System.out.println("------------------------------------------");

		for (int i = 0; i <= angka; i++) {
			bilangan = 0;
			for (int j = 1; j <= i; j++) {//
				if (i % j == 0) {
					bilangan = bilangan + 1;
				}
			}
			if (bilangan == 2) {
				System.out.print(i + " ");
			}
		}
	}

	private static void soal4() {
		System.out.println("Disini Soal 4");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");
		System.out.println();

		System.out.println("Input :");
		String angka = input.next();

		String[] arrayAngka = angka.split(":");

		int jam = Integer.parseInt(arrayAngka[0]);
		int menit = Integer.parseInt(arrayAngka[1]);
		int detik = Integer.parseInt(arrayAngka[2]);

		if (jam <= 12) {
			jam = jam + 12;

			if (menit < 10) {
				System.out.print(jam + ":" + 0 + menit + ":" + detik);
			}
			System.out.print(jam + ":" + menit + ":" + detik);
		} else {
			System.out.print(jam + ":" + menit + ":" + detik);
		}
	}

	private static void soal5() {
		System.out.println("Disini Soal 5");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");
		System.out.println();

		System.out.println("Input: ");
		int angka = input.nextInt();

		int bilPrima = 2;
		int result = 0;
		while (angka != 1) {
			result = angka;
			if (result % bilPrima == 0) {
				result = angka / bilPrima;
				System.out.println(angka + "/" + bilPrima + "=" + result);
				angka = result;
			} else {
				bilPrima++;
			}
		}

	}

	private static void soal6() {
		System.out.println("Disini Soal 6");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");
		System.out.println();

		System.out.println("jarak: ");
		int angka = input.nextInt();

	}

	private static void soal7() {
		System.out.println("Disini Soal 7");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");
		System.out.println();
		
		
		String sos = input.nextLine();
		char[] arrayCharSos = sos.toLowerCase().toCharArray();
		int temp = 0;

		// pengaman jika panjang inputan tidak sesuai 3 digit // sossop
		if (arrayCharSos.length % 3 > 0) {
			if (arrayCharSos.length % 3 == 1) {// jika 1 tidak 3 digit
				sos += "pp";
				arrayCharSos = sos.toLowerCase().toCharArray();
			} else if (arrayCharSos.length % 3 == 2) { // 2 digit
				sos += "p";
				arrayCharSos = sos.toLowerCase().toCharArray();
			}
		}

		// hitung error nya

		for (int i = 0; i < arrayCharSos.length; i += 3) {// P Q Q
			if (arrayCharSos[i] != 's') {
				temp++;
			}
			if (arrayCharSos[i + 1] != 'o') {
				temp++;
			}
			if (arrayCharSos[i + 2] != 's') {
				temp++;
			}
		}

		System.out.println(temp);
	}

		//jika perulangan menggunakan else if: tidak akan dilanjutkan 
		//jika perulangan menggunakan if: maka perhitungan akan dilanjutkan
	
}
