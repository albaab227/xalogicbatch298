package OOP;

import java.util.Scanner;

public class Angler{

	private static Scanner input = new Scanner(System.in);
	public static void main(String[] args) {
	
		
		Ikan ikan = new Ikan();
		Joran joran = new Joran();
		
		joran.joranMaguro();
		
		
		System.out.println("Jarak lemparan: ");
		int jarak = input.nextInt();
		
		System.out.println("Kekuatan orang: ");
		int kekuatan = input.nextInt();
		
		kekuatan += joran.kekuatanJoran;
		
		ikan.ikanLele();
		
		//wilayah hidup ikan
		if(jarak <= 5) {
			ikan.ikanLele();
		}else if(jarak <= 10){
			ikan.ikanGabus();
		}
		
		//kekuatan tarikan
		if(kekuatan >= ikan.kekuatan) {
			System.out.println("Mendapatkan ikan " + ikan.jenis);
		}else {
			System.out.println("ikan "+ikan.jenis+" lepas");
		}	
		
	}
	
}
