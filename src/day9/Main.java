package day9;


import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Main {
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Enter the numbeer of case");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {

				case 1:
					soal1();
					break;

				case 2:
					soal2();
					break;

				case 3:
					soal3();
					break;

				case 4:
					soal4();
					break;

				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;

				default:
					System.out.println("case is not available");
					break;

				}

			} catch (Exception e) {
				System.out.println(e.getMessage());

			}
			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}
	}

	private static void soal1() {
		System.out.println("Disini Soal 1");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");
		System.out.println();

	}

	private static void soal2() {
		System.out.println("Disini Soal 2");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");
		System.out.println();

		System.out.println("Keranjang yang dibawa: ");
		int keranjang = input.nextInt();
		String[] buah = new String[keranjang];

		System.out.println("Masukkan index keranjang yang dibawa: ");
		String index = input.next();
		String[] split = index.split(",");

		int sisa = 0;
		for (int i = 0; i < keranjang; i++) {

			System.out.println("Masukkan buah di keranjang " + (i + 1) + " : ");
			buah[i] = input.next();

			if (buah[i].equals("kosong")) {
				buah[i] = "0";
			}

			sisa = sisa + Integer.parseInt(buah[i]);

		}
		for (int i = 0; i < split.length; i++) {
			int nilai = Integer.parseInt(split[i]);
			sisa = sisa - Integer.parseInt(buah[nilai]);
		}
		System.out.println("Sisa buah: " + sisa);

	}

	private static void soal3() {
		System.out.println("Disini Soal 3");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");
		System.out.println();

		System.out.println("X= ");
		int orang = input.nextInt();

		int cara = 1;

		for (int i = 1; i <= orang; i++) {
			cara *= i;
		}
		System.out.println("Ada" + " " + cara + " " + "cara");
	}

	private static void soal4() {
		System.out.println("Disini Soal 4");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");
		System.out.println();

		System.out.println("Laki dewasa= ");
		String lakiDewasa = input.nextLine();
		System.out.println("Wanita dewasa= ");
		String wanitaDewasa = input.nextLine();
		System.out.println("Anak-anak= ");
		String anakAnak = input.nextLine();
		System.out.println("Bayi= ");
		String bayi = input.nextLine();

		int jumlahBaju = 0;
		int jumlahBajuDewasa = 0;
		int jumlahBajuWanita = 0;
		int jumlahBajuAnakAnak = 0;
		int jumlahBajuBayi = 0;
		if (Integer.parseInt(lakiDewasa) >= 0) {
			jumlahBajuDewasa = Integer.parseInt(lakiDewasa) * 1;
		}
		if (Integer.parseInt(wanitaDewasa) >= 0) {
			jumlahBajuWanita = Integer.parseInt(wanitaDewasa) * 2;
		}
		if (Integer.parseInt(anakAnak) >= 0) {
			jumlahBajuAnakAnak = Integer.parseInt(anakAnak) * 3;
		}
		if (Integer.parseInt(bayi) >= 0) {
			jumlahBajuBayi = Integer.parseInt(bayi) * 5;
		}
		jumlahBaju = jumlahBajuDewasa + jumlahBajuWanita + jumlahBajuAnakAnak + jumlahBajuBayi;
		if (jumlahBaju % 2 == 1 && jumlahBaju > 10) {
			jumlahBaju = jumlahBaju + (Integer.parseInt(wanitaDewasa) * 1);
		}
		System.out.println(jumlahBaju + " " + "Baju");

	}

	private static void soal5() {
		System.out.println("Disini Soal 5");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");
		System.out.println();

		System.out.print("Jumlah kue pukis : ");
		float totalKue = input.nextInt();

		System.out.println("Masukkan jumlah tepung terigu: ");
		float tepungTerigu = input.nextInt();
		System.out.println("Masukkan jumlah gula pasir: ");
		float gulaPasir = input.nextInt();
		System.out.println("Masukkan jumlah susu murni: ");
		float susuMurni = input.nextInt();
		System.out.println("Masukkan jumlah putih telur ayam: ");
		float putihTelurAyam = input.nextInt();

		float totalTepungTerigu = 0;
		float totalGulaPasir = 0;
		float totalSusuMurni = 0;
		float totalPutihTelurAyam = 0;
		
		NumberFormat formatter = new DecimalFormat("#0.000");
		
		if (totalKue > 0) {
			System.out.println("Jumlah bahan yang diperlukan untuk membuat 1 kue pukis: ");
			totalTepungTerigu = tepungTerigu / totalKue;
			totalGulaPasir = gulaPasir / totalKue;
			totalSusuMurni = susuMurni / totalKue;
			totalPutihTelurAyam = putihTelurAyam / totalKue;

			System.out.println("- " + "Tepung terigu: " + formatter.format(totalTepungTerigu) + " gram");
			System.out.println("- " + "Gula pasir: " + formatter.format(totalGulaPasir) + " gram");
			System.out.println("- " + "Susu murni: " + formatter.format(totalSusuMurni) + " gram");
			System.out.println("- " + "Putih telur ayam: " + formatter.format(totalPutihTelurAyam) + " gram");
		} else {
			System.out.println("Tidak ada kue yang mau dibuat");
		}

	}

	private static void soal6() throws ParseException {
		System.out.println("Disini Soal 6");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");
		

		}
		
		

	private static void soal7() {
		System.out.println("Disini Soal 7");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");
		System.out.println();
		
		

		
	}

	private static void soal8() {
		System.out.println("Disini Soal 8");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");
		System.out.println();

		System.out.print("Masukkan kata: ");
		String kata = input.next().toLowerCase();
		char[] arrayKata = kata.toCharArray();

		String temp = "";
		for (int i = arrayKata.length - 1; i >= 0; i--) {
			temp += arrayKata[i];
		}
		if (temp.equals(kata)) {
			System.out.println("Yes");
			
		} else {
			System.out.println("No");
		}

	}
}
