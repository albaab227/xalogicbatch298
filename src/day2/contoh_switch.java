package day2;

import java.util.Scanner;

public class contoh_switch {
			public static void main(String [] args) {
				
				Scanner input = new Scanner(System.in);
				System.out.println("Input Hadiah: ");
				
				int nomerHadiah = input.nextInt();
				
				switch(nomerHadiah) {
				
				case 1:
					System.out.println("Selamat Anda Mendapatkan Mobil");
					break;
				case 2:
					System.out.println("Selamat Anda Mengdapatkan Tiket Pulang ke Bandung");
					break;
				case 3:
					System.out.println("Selamat Anda Mendapatkan Boneka Beruang");
					break;
				case 4:
					System.out.println("Selamat Anda Mendapatkan Liburan ke India");
					break;
				case 5:
					System.out.println("Selamat Anda Mendapatkan Motor Harley Jawa");
					break;
				default:
					System.out.println("Nomer yang anda pilih tidak sesuai");
					break;
					
					
				}
				
			}
}
