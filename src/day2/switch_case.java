package day2;

import java.util.Scanner;

public class switch_case {
		public static void main(String [] args) {
		
		//membuat variabel dan Scanner
		String lampu;
		Scanner scan = new Scanner(System.in);
		
		//mengambil input
		System.out.println("Input Nama Warna");
		lampu = scan.nextLine();
		
		switch(lampu) {
			case "merah":
				System.out.println("Lampu Merah, Berhenti!");
				break;
			case "Kuning":
				System.out.println("Lampu Kuning, Harap hati - hati");
				break;
			case "Merah":
				System.out.println("Lampu Merah, Silahkan Jalan!");
				break;
			default:
				System.out.println("Warna Lampu Salah !");
				}
			}
}
