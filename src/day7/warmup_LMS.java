package day7;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Scanner;

public class warmup_LMS {
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Enter the numbeer of case");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {

				case 3:
					soal3();
					break;

				case 4:
					soal4();
					break;

				case 5:
					soal5();
					break;

				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;

				default:
					System.out.println("case is not available");
					break;

				}

			} catch (Exception e) {
				System.out.println(e.getMessage());

			}
			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}
	}

	private static void soal3() {
		System.out.println("Disini Soal 3");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");
		System.out.println();

		System.out.print("Input=");
		String angka = input.nextLine();
		String[] arrayAngka = angka.split(" ");

		int hasil = 0;
		// hasil penambahan index
		for (int i = 0; i < arrayAngka.length; i++) {

			hasil = hasil + Integer.parseInt(arrayAngka[i]);

		}
		System.out.print(hasil);

	}

	private static void soal4() {
		System.out.println("Disini Soal 4");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");
		System.out.println();

		System.out.print("Inputkan nilai n untuk matrix (nxn) : ");
		int n = input.nextInt();
		int [][] matrix = new int[n][n];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				System.out.print("inputkan value array ke - " + i + " " + j + " : ");
				int value = input.nextInt();
				matrix[i][j] = value;
			}
		}
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println("");
		}
		System.out.println("");
		int primer = 0;
		int sekunder = 0;
		int diagonalDifferce = 0;
		for (int i = 0; i < n; i++)
	    {
	        for (int j = 0; j < n; j++)
	        {
	            if (i == j)
	                primer += matrix[i][j];
	            if (i == n - j - 1)
	                sekunder += matrix[i][j];
	        }
	    }
		System.out.println("");
		System.out.println("primer : " + (primer));
		System.out.println("sekunder : " + (sekunder));
		diagonalDifferce = Math.abs(primer) - Math.abs(sekunder);
		diagonalDifferce = Math.abs(diagonalDifferce);
		System.out.println("diference : " + (diagonalDifferce));		
	}


	private static void soal5() {
		System.out.println("Disini Soal 5");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");
		System.out.println();

		System.out.println("Input= ");
		String angka = input.nextLine();
		System.out.println("pembagi= ");
		int pembagi = input.nextInt();

		String[] arrayAngka = angka.split(" ");
		int negative = 0;
		int positif = 0;
		int zero = 0;
		String hasilPositif;
		String hasilNegative;
		String hasilZero;

		// menentukan total positif, negatif,zero
		for (int i = 0; i < arrayAngka.length; i++) {

			if (Integer.parseInt(arrayAngka[i]) > 0) {
				positif++;
			} else if (Integer.parseInt(arrayAngka[i]) == 0) {
				zero++;
			} else {
				negative++;
			}

		}
		// hasil bilangan desimal
		DecimalFormat decimalFormat = new DecimalFormat("0.000000");
		hasilNegative = decimalFormat.format((double) (negative) / pembagi);
		hasilPositif = decimalFormat.format((double) (positif) / pembagi);
		hasilZero = decimalFormat.format((double) (zero) / pembagi);

		System.out.println("total negative= " + hasilPositif);
		System.out.println("total negative= " + hasilNegative);
		System.out.println("total negative= " + hasilZero);
	}

	private static void soal6() {
		System.out.println("Disini Soal 6");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");
		System.out.println();

		System.out.println("Masukkan n: ");
		int n = input.nextInt();

		String temp = "";
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (i + j >= n - 1) {
					temp += "#";
				} else {
					temp += "   ";
				}
			}
			temp += "\n";
		}

		// output
		System.out.println(temp);

	}

	private static void soal7() {
		System.out.println("Disini Soal 7");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");
		System.out.println();

		// input

		System.out.println("Masukkan array: ");
		String string = input.nextLine();

		// mengubah string ke array berdasarkan spasi
		String[] arrayString = string.split(" ");

		// inisialisasi nilai awal max dan min
		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;
		for (int i = 0; i < arrayString.length; i++) {
			int jumlahAngka = 0;
			for (int j = 0; j < arrayString.length; j++) {
				if (i != j) {
					jumlahAngka += Integer.parseInt(arrayString[j]);
				}
			}
			// menyimpan nilai tertinggi
			if (max < jumlahAngka) {
				max = jumlahAngka;
			}
			// menyimpan nilai terendah
			if (min > jumlahAngka) {
				min = jumlahAngka;
			}
		}
		System.out.println(max + " " + min);

	}

	private static void soal8() {
		System.out.println("Disini Soal 8");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");
		System.out.println();
		//tinggi lilin
		System.out.println("Input=  ");
		String angka = input.nextLine();

		String[] arrayAngka = angka.split(" ");

		// cari nilai tertinggi
		int max = 0;
		for (int i = 0; i < arrayAngka.length; i++) {
			if (max < Integer.parseInt(arrayAngka[i])) {
				max = Integer.parseInt(arrayAngka[i]);
			}
		}
		// jumlah nilai tertinggi
		int totalTertinggi = 0;
		for (int i = 0; i < arrayAngka.length; i++) {
			if (max == Integer.parseInt(arrayAngka[i])) {
				totalTertinggi++;

			}
		}

		System.out.println(totalTertinggi);

	}

	private static void soal9() {
		System.out.println("Disini Soal 9");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");
		System.out.println();

		System.out.println(" ");
		String angka = input.nextLine();

		String[] arrayAngka = angka.split(" ");

		int temp = 0;

		BigDecimal temp1 = new BigDecimal(temp);

		for (int i = 0; i < arrayAngka.length; i++) {

			temp1 = temp1.add(new BigDecimal(arrayAngka[i]));
		}
		System.out.println(temp1);
	}

	private static void soal10() {
		System.out.println("Disini Soal 10");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");
		System.out.println();

		System.out.println(" ");
		String angka1 = input.nextLine();
		System.out.println(" ");
		String angka2 = input.nextLine();

		String[] arrayAngka1 = angka1.split(" ");
		String[] arrayAngka2 = angka2.split(" ");

		int pointAlice = 0;
		int pointBob = 0;
		// panjang index alice
		for (int i = 0; i < arrayAngka1.length; i++) {
			int alice = 0;

			// panjang index bob
			for (int j = 0; j < arrayAngka2.length; j++) {
				int bob = 0;

				if (i == j) {
					alice = Integer.parseInt(arrayAngka1[i]);
					bob = Integer.parseInt(arrayAngka2[j]);

					if (alice < bob) {
						pointBob++;
					} else if (alice > bob) {
						pointAlice++;

					}
				}

			}

		}
		System.out.println(pointAlice + " " + pointBob);
	}
}
