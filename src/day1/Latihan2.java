package day1;

public class Latihan2 {
	public static void main(String [] args) {
		System.out.println("Selamat Pagi");
		System.out.println("Selamat Datang");
		
		boolean mauKeluar = true;
		if (mauKeluar) {
			System.out.println("Sampai Jumpa");
		}else
			System.out.println("Tidak Jadi Keluar");
		
		// Batch 298 Lulus jika memiliki nilai lebih besar
		// atau sama dengan 75
		
		int nilaiAcheng = 75;
		int nilaiSoftSkill = 3;
		
		if (nilaiAcheng >= 75 && nilaiSoftSkill >= 3) { // true
			// statement
			System.out.println("Kucheng Lulus Bootcamp"); 
		}else {
			System.out.println("Acheng ke Bangung");
		}
	}}
