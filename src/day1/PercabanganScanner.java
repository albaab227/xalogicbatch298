package day1;

import java.util.Scanner;

public class PercabanganScanner {
	
	public static void main(String [] args) {
		Scanner input = new Scanner(System.in);  
		// untuk import klik scanner ctrl+space
		
		// Batch 298 Lulus jika memiliki nilai lebih besar
		// atau sama dengan 75
		
		System.out.println("Masukkan Nama Siswa");
		String nama = input.next();
		System.out.println("Masukkan Nilai Siswa: ");
		int nilai = input.nextInt();
		System.out.println("Masukkan Nilai Softskill : ");
		int nilaiSoftSkill = input.nextInt();
		
		input.nextLine();// untuk perubahan tipe data dari integer ke string agar tidak ke skip
		System.out.println("Masukkan Batch");
		String Batch = input.nextLine(); //untuk menginput string yang bisa menggunakan spasi
		
		
		if (nilai >= 75 && nilaiSoftSkill >= 3) { // true
			// statement
			System.out.println(nama + " "  + Batch  + " " + "Lulus Bootcamp"); 
		}else {
			System.out.println(nama + " "  + Batch  + " " + "Tidak Lulus Bootcamp");
		}
	}}

