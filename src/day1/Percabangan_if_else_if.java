package day1;

import java.util.Scanner;

public class Percabangan_if_else_if {
	
	public static void main(String [] args) {
		Scanner input = new Scanner(System.in);  
		// untuk import klik scanner ctrl+space
		
		// Batch 298 Lulus jika memiliki nilai lebih besar
		// atau sama dengan 75
		
		System.out.println("Masukkan Nama Siswa");
		String nama = input.nextLine();
		System.out.println("Masukkan Nilai Siswa: ");
		int nilai = input.nextInt();
		System.out.println("Masukkan SoftSkill: ");
		int nilaiSoftSkill = input.nextInt();
		input.nextLine();// untuk perubahan tipe data dari integer ke string agar tidak ke skip
		System.out.println("Masukkan Batch");
		String batch = input.nextLine(); //untuk menginput string yang bisa menggunakan spasi
		String grade=" ";
		String Kelulusan=" ";
		//Nilai >= 90 = A, Kelulusan = LULUS
		//Nilai >= 85 = B+, Kelulusan = LULUS
		//Nilai >= 80 = B, Kelulusan = LULUS
		//Nilai >= 75 = C+, Kelulusan = LULUS
		//Nilai >= 70 = C, Kelulusan = REMEDIAL
		//Nilai <= 70 = D, Kelulusan = TIDAK LULUS
		//nama, batch kelulusan, grade, kelulusan
		
			
		if (nilai >= 90) { // true
			// statement
			grade = "A";
			Kelulusan = "LULUS";
		} else if (nilai >= 85) {
			grade = "B+";
			Kelulusan = "LULUS";
		} else if (nilai >= 80) {
			grade = "B";
			Kelulusan = "LULUS";
		} else if (nilai >= 75) {
			grade = "C+";
			Kelulusan = "LULUS";
		} else if (nilai >= 70) {
			grade = "C";
			Kelulusan = "REMEDIAL";
		} else if (nilai <= 70 && nilaiSoftSkill >= 3) {
			grade = "D";
			Kelulusan = "TIDAK LULUS";
		
		System.out.println(nama + " , " + batch + "  " + grade + "  " + Kelulusan + "  " + "Tidak Lulus Bootcam"  );
		}
	}
}

