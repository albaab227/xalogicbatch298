package day3;

import java.util.Scanner;

public class Nested_piramit {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("M = ");
		int m = input.nextInt();

		for (int i = 0; i < m; i++) {
			for (int j = 0; j < m; j++) {
				if (i>=j ) {
					if( i+j <= m-1) {
						System.out.print("*");
					}
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
		
		System.out.println("=====================================");
		
		for (int i=0; i<m; i++) {
			for(int j=0; j<m; j++) {
				if (i+j >= m-1) {
					System.out.print("*");
				}else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}
}
