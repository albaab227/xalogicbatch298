package day3;

import java.util.Scanner;

public class while_looping {

	public static void main(String[] args) {
		 Scanner input = new Scanner (System.in);
		 
		 System.out.println("Nilai N: ");
		 int angka =input.nextInt();
		 
		int i = 0;
		
		
		while(i < angka) {
			System.out.print(i+1 + " ");
			i++;
			
		}
	}
}
