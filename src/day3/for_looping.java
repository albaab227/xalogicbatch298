package day3;

import java.util.Scanner;

public class for_looping {
		
		public static void main (String[] args) {
			
			Scanner input = new Scanner(System.in);
	    	 
	    	 System.out.println("nilai N = ");
	    	 int angka = input.nextInt();
	    	 
	    	 
	    	 for ( int i = 0; i < angka; i ++ ) {
	    		 System.out.print(i + 1);
	    	 }
	    	 
	    	 System.out.println(" ");
	    	 
	    	 for ( int i = angka; i > 0; i-- ) {
	    		 System.out.print(i);
	    		 
	    		
	    	 }
		}
	
}
