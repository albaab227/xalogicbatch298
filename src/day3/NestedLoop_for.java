package day3;

import java.util.Scanner;

public class NestedLoop_for {
	public static void main(String[] args) {
		Scanner input =new Scanner(System.in);
		System.out.println("M = ");
		int m = input.nextInt();
			
		for (int i = 0; i < m; i++) { // baris
			for (int j = 0; j < m; j++) { // kolom
				if (i == j || i+j == m-1) {
					System.out.print("*");
					
				}else {
					System.out.print(" ");
				}

			}
			System.out.println();
		}
	}
}