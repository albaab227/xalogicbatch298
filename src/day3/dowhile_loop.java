package day3;

import java.util.Scanner;

public class dowhile_loop {
		public static void main (String [] args) {
			Scanner input = new Scanner (System.in);
			System.out.println("Masukkan Nilai N: ");
			int n = input.nextInt(); 
			int i=0;
			
			do {
				if(i==n-1) {
					System.out.println(i+1); //5
					
				}else {	
				System.out.print((i+1) + " , "); //1, 2, 3, 4,
				}
				i++; //i= i+1 =1; i=2, i=3, i=4, i=5
				
			}while (i<10);
			}
}
