package day5;

import java.util.Scanner;

public class Main {
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Enter the numbeer of case");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {

				case 1:
					soal1();
					break;

				case 2:
					soal2();
					break;

				case 3:
					soal3();
					break;

				case 4:
					soal4();
					break;

				case 5:
					soal5();
					break;

				case 6:
					soal6();
					break;

				default:
					System.out.println("case is not available");
					break;

				}

			} catch (Exception e) {
				System.out.println(e.getMessage());

			}
			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}
	}

	private static void soal1() {
		System.out.println("Disini Soal 1");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");
		System.out.println();

		System.out.println("Uang Andi= ");
		int uang = input.nextInt();

		System.out.println("Harga Baju= ");
		String baju = input.next();

		System.out.println("Harga Celana= ");
		String celana = input.next();

		String[] arrayBaju = baju.split(",");
		String[] arrayCelana = celana.split(",");

		int total;
		int temp = 0;

		for (int i = 0; i < arrayBaju.length; i++) {
			for (int j = 0; j < arrayCelana.length; j++) {
				if (i == j) {
					// mengubah array string ke array integer
					total = Integer.parseInt(arrayBaju[i]) + Integer.parseInt(arrayCelana[j]);
					if (total <= uang && total >= temp) {

						temp = total;
					}
				}
			}
		}
		System.out.println(temp);

	}

	private static void soal2() {
		System.out.println("Disini Soal 2");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");
		System.out.println();

		System.out.println("arr= ");
		String angka = input.nextLine();

		System.out.println("rot= ");
		int lompat = input.nextInt();

		String[] arrayAngka = angka.split(",");
		String temp = "";

		for (int i = 0; i < lompat; i++) {
			for (int j = 0; j < arrayAngka.length; j++) {
				if (j == arrayAngka.length - 1) {
					temp = temp + arrayAngka[0];
				} else {
					temp = temp + arrayAngka[j + 1] + ", ";
				}
			}
			System.out.print((i + 1) + ": ");
			System.out.println(temp);

			arrayAngka = temp.split(",");
			temp = " ";
		}

	}

	private static void soal3() {
		System.out.println("Disini Soal 3");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");
		System.out.println();

		System.out.println("Puntung Rokok= "); // output batang rokok
		int jumlahPuntungRokok = input.nextInt(); // inputan jumlah rokok yg di dapat

		int batangRokok = 0; // variabel rokok
		int sisaPuntung = 0; // variabel puntung rokok yg di dapat
		int puntungRokok = 8; // variabel 8 puntung rokok = 1 batang rokok
		int harga = 500; // variabel harga rokok setiap batang rokok

		batangRokok = jumlahPuntungRokok / puntungRokok; // hasil puntung rokok
		sisaPuntung = jumlahPuntungRokok % puntungRokok; // hasil sisa puntung rokok
		harga = harga * batangRokok; // total harga 1 puntung rokok

		System.out.println("Batang Rokok= " + batangRokok);
		System.out.println("sisa putung rokok= " + sisaPuntung);
		System.out.println("Harga rokok= " + harga);
	}

	private static void soal4() {
		System.out.println("Disini Soal 4");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");

		System.out.println("Total Menu= ");
		int totalMenu = input.nextInt();

		System.out.println("Makan alergi Index ke- ");
		int alergi = input.nextInt();
		input.nextLine();

		System.out.println("Harga Menu=  ");
		String hargaMenu = input.next();

		System.out.println("Uang Elsa= ");
		int uang = input.nextInt();

		int temp = 0;
		int sisa = 0;

		String[] arrayHargaMenu = hargaMenu.split(",");

		if (totalMenu != arrayHargaMenu.length) {
			System.out.println("Total  menu dan total harga nemu tidak sesuai");
			return;// fungsi kembali
		}

		for (int i = 0; i < arrayHargaMenu.length; i++) {
			if (i != alergi) {
				temp += Integer.parseInt(arrayHargaMenu[i]);

			}
		}
		temp /= 2;
		System.out.println("Elsa harus membayar: Rp. " + temp + ",-");

		sisa = uang - temp;

		if (sisa < 0) {

			System.out.println("Else mengutang dimas: " + Math.abs(sisa));

		} else if (sisa == 0) {
			System.out.println("Elsa tidak memiliki sisa uang");
		} else {
			System.out.println("sisa uang Elsa: Rp. " + sisa + ",-");
		}
	}

	private static void soal5() {
		System.out.println("Disini Soal 5");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

		System.out.print("Input= ");
		String words = input.nextLine().toLowerCase().replaceAll(" ", "");
		char vokal = ' ';
		char konsonan = ' ';
		char[] arrayChar = words.toCharArray();

		for (int i = 0; i < arrayChar.length; i++) {
			if (arrayChar[i] == 'a' || arrayChar[i] == 'i' || arrayChar[i] == 'u' || arrayChar[i] == 'e'
					|| arrayChar[i] == 'o') {
				vokal = arrayChar[i];
				int a = 0;
				a = vokal + a;
				System.out.println("Vokal: " + vokal);
			} else {
				konsonan = arrayChar[i];
				System.out.println("konsonan: " + konsonan);
			}

		}

	}

	private static void soal6() {
		System.out.println("Disini Soal 6");
		System.out.println("XXXXXXXXXXXXXXXXXXXXXX");

		System.out.print("Masukkan kalimat: ");// untuk menginputkan suatu kalimat
		String kalimat = input.nextLine(); // variabel kalimat type datanya string
		String[] arrayHuruf = kalimat.split(" ");
		String temp = ""; //
		char charTemp = 0;

		for (int i = 0; i < arrayHuruf.length; i++) { // perulangan untuk array huruf
			char[] charArrayKalimat = arrayHuruf[i].toCharArray(); // untuk mengubah array huruf menjadi array char
			for (int j = 0; j < charArrayKalimat.length; j++) { // perulangan untuk array char
				charTemp = charArrayKalimat[j];
				if (j % 2 == 1) { // percabangan char array
					temp += "*"; // kondisi jika benar
				} else {
					temp += String.valueOf(charTemp); // kondisi jika salah
				}
			}
			if (i == arrayHuruf.length - 1) {
				temp += "";
			} else {
				temp += " ";
			}
		}
		System.out.println(temp);
	}
}
