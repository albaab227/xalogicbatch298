package day5;

public class StringManipulation {
		public static void main (String [] args) {
			
			//String Manipulation
			
			// memisahkan kalimat menjadi perkata (menggunakan split)
			String words1 = "Andre Muhammad Satrio";
			String[] tempWords1 = words1.split(" ");
			System.out.println(tempWords1[2]); //menampilkan kata satrio
			
			
			//mengubah kata menjadi karakter (menggunakan toCharArray)
			char[] charWords1 = tempWords1[2].toCharArray();
			System.out.println(charWords1[charWords1.length - 1]);
			
			
			//mengganti character dari kalimat
			String replaceWords1 = words1.replaceAll(" ", "");
			System.out.println(replaceWords1);
			
			
			//memecah menjadi character
			char[] charReplaceWords1 = replaceWords1.toCharArray();
			System.out.println(charReplaceWords1[17]);
			
			
			//untuk menampulkan kata muhammad
			String muhammad =words1.substring(6,14);
			System.out.println(muhammad);
		
			
			//
			String lowerWords1 = words1.toLowerCase().replaceAll(" ", "");
			String tempMuhammad =lowerWords1.substring(5,13);
			System.out.println(tempMuhammad);
			
			
			//untuk mengubah huruf kapital menjadi huruf kecil
			char[] tempLowerWords1 = lowerWords1.toCharArray();
			System.out.println(lowerWords1);
			
			
			//untuk mengubah huruf kecil menjadi huruf kapital
			String upperWords1 = words1.toUpperCase();
			System.out.println(upperWords1);
			
			
		}
		
		
}
