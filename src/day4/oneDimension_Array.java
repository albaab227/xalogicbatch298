package day4;

import java.util.Scanner;

public class oneDimension_Array {
		public static void main (String [] args) {
			
			String[] nama = new String[3];
			
			Scanner input = new Scanner(System.in);
			
				for( int i=0; i< nama.length; i++) {
					System.out.print("Nama ke - " + i + ": ");
					nama[i] = input.nextLine();
					
				}
				System.out.println("=================");
				
				//menampilkan isi array
				for(String b : nama) {
					System.out.println(b);
				}
			
		}
}
