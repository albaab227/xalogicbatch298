package day4;

import java.util.Scanner;

public class Soal_7_OneDimension {
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.print("n= ");
		int nilai = input.nextInt();
		int[] arrayAngka = new int[nilai];
		int i;
		int t = 2;
		for (i = 0; i <arrayAngka.length; i++) {
			arrayAngka[i]=t;
			System.out.print(arrayAngka[i] + " ");
			t*=2;
		}
		System.out.println();
	}
}
