package day4;

import java.util.Scanner;

public class Soal_5_OneDimension {
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.print("n= ");
		int nilai = input.nextInt();

		int[] arrayAngka = new int[nilai];
		int i = 0;

		int t = 0;
		for (i = 0; i < nilai; i++) {
			t = i * 4 + 1;

			System.out.print(t + " ");

			if (t == 5) {
				System.out.print(" * ");
			}
			if (t == 13) {
				System.out.print(" * ");
			} else {
				System.out.print(" ");
			}

		}
		System.out.println();
	}
}
