package day4;

import java.util.Scanner;

public class Soal_3_OneDimension {
				public static void main (String[]args) {
					
					Scanner input = new Scanner (System.in);
					
					System.out.print("n= ");
					int nilai = input.nextInt();
					int[] arrayAngka = new int[nilai];
					
					int t=0;
					for(int i=0; i<nilai; i++) {
						t=i*3+1;
						arrayAngka[i]= t;
						
						System.out.print(arrayAngka[i]+ " ");
					}
					System.out.println();
				}
}
