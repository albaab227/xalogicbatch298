package day4;

import java.util.Scanner;

public class Soal_8_OneDimension {
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.print("n= ");
		int nilai = input.nextInt();
		int[] arrayAngka = new int[nilai];
		int i;
		int t = 3;
		for (i = 0; i <arrayAngka.length; i++) {
			arrayAngka[i]=t;
			System.out.print(arrayAngka[i] + " ");
			t*=3;
		}
		System.out.println();
	}
}
