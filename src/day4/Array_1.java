package day4;

public class Array_1 {
	public static void main(String[] args) {

		int[] arrayAngka = new int[5];
		String dua = "2";

		int tempAngka = Integer.parseInt(dua); // convert / mengubah String ke integer

		arrayAngka[0] = 1;
		arrayAngka[1] = tempAngka; // memasukan data ke dalam array
		arrayAngka[2] = 2;
		arrayAngka[3] = 3;

		int aksesAngka = arrayAngka[1]; // akses nilai di suatu array

		for (int i = 0; i < arrayAngka.length; i++) {
			System.out.print(arrayAngka[i] + " ");

		}
		System.out.println();

		System.out.println("Panjang Array" + arrayAngka.length);
		System.out.println(aksesAngka);

		String[] arrayPembelian = new String[5];
		
		int pembelianPertama = 10000;
		int pembelianKedua = 20000;
		
		//convert /mengubah integer ke String (cara ke-1)
		arrayPembelian[0] = String.valueOf(pembelianPertama);
		
		//convert/ mengubah integer ke string (cara ke-2)
		arrayPembelian[1] = pembelianKedua + "";
		
		
		System.out.println("Nilai Pembelian= " + arrayPembelian[0]);
		System.out.println("Nilai Pembelian= " + arrayPembelian[1]);
	}
}